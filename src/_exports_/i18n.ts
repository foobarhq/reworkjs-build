export * from '../framework/common/i18n/index.js';
export { useActiveLocale } from '../framework/common/active-locale-context.js';
export type { TActiveLocaleContext } from '../framework/common/active-locale-context.js';
