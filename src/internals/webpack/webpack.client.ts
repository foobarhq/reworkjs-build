import WebpackBase from './WebpackBase.js';

export default new WebpackBase(WebpackBase.SIDE_CLIENT).buildConfig();
