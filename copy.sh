# cp -r ../react-framework/lib/ lib
# cp -r ../react-framework/package.json package.json
cd ../core
npm run build
cd ../reworkjs-build

rm -rf lib src es
rsync -ar --progress ../core/ ./ --exclude 'node_modules' --exclude '.git' --exclude '.gitignore'
rm -rf .git
git init
git add --all
git commit -n -m "build"

git checkout -B develop
git remote add origin git@bitbucket.org:foobarhq/reworkjs-build.git
git push --force origin develop
