const listenerMap = new WeakMap();
export function listenMsg(proc, msgType, callback) {
  if (!listenerMap.has(proc)) {
    bindMessageHandler(proc);
  }

  return addMessageListener(proc, msgType, callback);
}

function bindMessageHandler(proc) {
  proc.on('message', data => {
    var _listenerMap$get;

    if (!listenerMap.has(proc)) {
      return;
    }

    if (data == null || typeof data !== 'object' // @ts-expect-error
    || typeof data.cmd !== 'string') {
      return;
    } // @ts-expect-error


    const msgType = data.cmd;
    const listeners = (_listenerMap$get = listenerMap.get(proc)) == null ? void 0 : _listenerMap$get.get(msgType);

    if (!listeners) {
      return;
    }

    for (const listener of listeners) {
      listener(data);
    }
  });
}

function addMessageListener(proc, msgType, callback) {
  const listeners = listenerMap.get(proc) ?? new Map();

  if (!listeners.has(msgType)) {
    listeners.set(msgType, []);
  }

  listeners.get(msgType).push(callback);
  listenerMap.set(proc, listeners);
}