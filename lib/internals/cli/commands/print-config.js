import frameworkConfig from '@reworkjs/core/_internal_/framework-config';
import logger from '@reworkjs/core/logger';
import noop from 'lodash/noop.js';
export default function registerCommand(cli) {
  cli.command('print-config', 'Prints out the parsed configuration of the framework', noop, () => {
    logger.info('framework configuration:');
    logger.info(`\n${JSON.stringify(frameworkConfig, null, 2)}`);
  });
}