import childProcess from 'child_process';
export function execSync(cmd) {
  return childProcess.execSync(cmd, {
    env: process.env,
    stdio: 'inherit'
  });
}