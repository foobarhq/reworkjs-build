import glob from 'glob';
export async function asyncGlob(path, opts) {
  if (opts === void 0) {
    opts = {};
  }

  return new Promise((resolve, reject) => {
    glob(path, opts, (error, files) => {
      if (error) {
        return void reject(error);
      }

      resolve(files);
    });
  });
}