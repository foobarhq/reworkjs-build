import _classPrivateFieldLooseBase from "@babel/runtime/helpers/classPrivateFieldLooseBase";
import _classPrivateFieldLooseKey from "@babel/runtime/helpers/classPrivateFieldLooseKey";
import escapeRegExp from 'escape-string-regexp';
import merge from 'lodash/merge.js';
import pushAll from '../../shared/util/push-all.js';
const FILE_TYPE_JS = Symbol('FILE_TYPE_JS');
const FILE_TYPE_CSS = Symbol('FILE_TYPE_JS');
const FILE_TYPE_IMG = Symbol('FILE_TYPE_IMG');
const stateHolder = new WeakMap();

function getState(instance) {
  return stateHolder.get(instance);
}

var _extraEntries = /*#__PURE__*/_classPrivateFieldLooseKey("extraEntries");

export default class WebpackConfigBuilder {
  constructor() {
    Object.defineProperty(this, _extraEntries, {
      writable: true,
      value: Object.create(null)
    });
    const state = {
      fileTypes: {
        [FILE_TYPE_JS]: ['js', 'jsx', 'mjs', 'ts', 'tsx'],
        [FILE_TYPE_CSS]: ['css'],
        [FILE_TYPE_IMG]: ['jpg', 'jpeg', 'png', 'gif', 'svg', 'webp']
      },
      rawConfig: {},
      rules: [],
      cssLoaders: [],
      plugins: [],
      aliases: {}
    };
    stateHolder.set(this, state);
  }

  addEntry(name, entry) {
    if (_classPrivateFieldLooseBase(this, _extraEntries)[_extraEntries][name] || name === 'main') {
      throw new Error('Entry already defined');
    }

    _classPrivateFieldLooseBase(this, _extraEntries)[_extraEntries][name] = entry;
    return this;
  }

  getEntries() {
    return _classPrivateFieldLooseBase(this, _extraEntries)[_extraEntries];
  }

  registerFileType(type, extension) {
    pushAll(getState(this).fileTypes[type], extension);
  }

  injectCssLoader(loader) {
    // loader execution order is last to first.
    getState(this).cssLoaders.unshift(loader);
  }

  injectRawConfig(config) {
    merge(getState(this).rawConfig, config);
  }

  injectPlugins(plugins) {
    pushAll(getState(this).plugins, plugins);
  }

  injectRules(rule) {
    pushAll(getState(this).rules, rule);
  }

  injectAlias(original, alias) {
    getState(this).aliases[original] = alias;
  }

}
WebpackConfigBuilder.FILE_TYPE_JS = FILE_TYPE_JS;
WebpackConfigBuilder.FILE_TYPE_CSS = FILE_TYPE_CSS;
WebpackConfigBuilder.FILE_TYPE_IMG = FILE_TYPE_IMG;
export function getAliases(instance) {
  return getState(instance).aliases;
}
export function getPlugins(instance) {
  return getState(instance).plugins;
}
export function mergeRaw(instance, obj) {
  return merge(obj, getState(instance).rawConfig);
}
export function buildRules(instance) {
  const state = getState(instance);
  return state.rules.map(_obj => {
    const obj = Object.assign({}, _obj); // map the file type symbols to their respective regexp.
    // this allows adding extensions for some files after the rules was added.

    obj.test = getFileTypeRegExp(instance, obj.test);
    return obj;
  });
}
export function getCssLoaders(instance) {
  return getState(instance).cssLoaders;
}
export function getFileTypeExtensions(instance, fileType) {
  const state = getState(instance);

  if (!state.fileTypes[fileType]) {
    throw new TypeError('Invalid filetype');
  }

  return state.fileTypes[fileType];
}
export function getFileTypeRegExp(instance, fileType) {
  if (fileType instanceof RegExp) {
    return fileType;
  }

  return toRegExp(getFileTypeExtensions(instance, fileType));
}

function toRegExp(fileExt) {
  return new RegExp(`\\.(${fileExt.map(escapeRegExp).join('|')})$`, 'i');
}