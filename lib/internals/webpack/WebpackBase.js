import _classPrivateFieldLooseBase from "@babel/runtime/helpers/classPrivateFieldLooseBase";
import _classPrivateFieldLooseKey from "@babel/runtime/helpers/classPrivateFieldLooseKey";
var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/internals/webpack/WebpackBase.tsx";
import assert from 'assert';
import { createRequire } from 'module';
import path from 'path';
import LoadablePlugin from '@loadable/webpack-plugin';
import frameworkConfig from '@reworkjs/core/_internal_/framework-config';
import frameworkMetadata from '@reworkjs/core/_internal_/framework-metadata';
import logger from '@reworkjs/core/logger';
import projectMetadata from '@reworkjs/core/project-metadata';
import CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import minifiedCssIdents from 'mini-css-class-name/css-loader/index.js';
import ExtractCssPlugin from 'mini-css-extract-plugin';
import { renderToString } from 'react-dom/server.js';
import { HelmetProvider } from 'react-helmet-async';
import ResolveTypescriptPlugin from 'resolve-typescript-plugin';
import webpack from 'webpack';
import nodeExternals from 'webpack-node-externals';
import SriPlugin from 'webpack-subresource-integrity';
import WebpackBar from 'webpackbar';
import BaseHelmet from '../../framework/common/BaseHelmet.js';
import appArgv from '../../framework/common/app-argv/node.js';
import renderPage from '../../framework/server/setup-http-server/render-page.js';
import { chalkNok, chalkOk } from '../../shared/chalk.js';
import getWebpackSettings from '../../shared/webpack-settings.js';
import argv from '../rjs-argv.js';
import { resolveFrameworkSource } from '../util/resolve-util.js';
import WebpackConfigBuilder, * as wcbUtils from './WebpackConfigBuilder.js';
import featureClasses from './features.js';
import sortDependencies from './sort-dependencies.js'; // TODO: remove once import.meta.resolve is ready

import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";

const require = createRequire(import.meta.url);

const isDev = process.env.NODE_ENV === 'development';
const isTest = process.env.NODE_ENV === 'test';

function parseFeatures(str) {
  if (!str) {
    return {};
  }

  const features = {};

  for (const featureName of str.split(',')) {
    if (featureName.startsWith('-')) {
      features[featureName.substring(1)] = false;
    } else {
      features[featureName] = true;
    }
  }

  return features;
}

var _webpackConfigBuilder = /*#__PURE__*/_classPrivateFieldLooseKey("webpackConfigBuilder");

var _injectFeatures = /*#__PURE__*/_classPrivateFieldLooseKey("injectFeatures");

var _injectFeature = /*#__PURE__*/_classPrivateFieldLooseKey("injectFeature");

var _getMainEntry = /*#__PURE__*/_classPrivateFieldLooseKey("getMainEntry");

var _getOutput = /*#__PURE__*/_classPrivateFieldLooseKey("getOutput");

var _buildRules = /*#__PURE__*/_classPrivateFieldLooseKey("buildRules");

var _getCssLoader = /*#__PURE__*/_classPrivateFieldLooseKey("getCssLoader");

var _buildCssLoaders = /*#__PURE__*/_classPrivateFieldLooseKey("buildCssLoaders");

var _getAliases = /*#__PURE__*/_classPrivateFieldLooseKey("getAliases");

var _getDefinedVars = /*#__PURE__*/_classPrivateFieldLooseKey("getDefinedVars");

var _getPlugins = /*#__PURE__*/_classPrivateFieldLooseKey("getPlugins");

export default class WebpackBase {
  /** @private */

  /** @private */

  /** @private */
  constructor(side) {
    Object.defineProperty(this, _getPlugins, {
      value: _getPlugins2
    });
    Object.defineProperty(this, _getDefinedVars, {
      value: _getDefinedVars2
    });
    Object.defineProperty(this, _getAliases, {
      value: _getAliases2
    });
    Object.defineProperty(this, _buildCssLoaders, {
      value: _buildCssLoaders2
    });
    Object.defineProperty(this, _getCssLoader, {
      value: _getCssLoader2
    });
    Object.defineProperty(this, _buildRules, {
      value: _buildRules2
    });
    Object.defineProperty(this, _getOutput, {
      value: _getOutput2
    });
    Object.defineProperty(this, _getMainEntry, {
      value: _getMainEntry2
    });
    Object.defineProperty(this, _injectFeature, {
      value: _injectFeature2
    });
    Object.defineProperty(this, _injectFeatures, {
      value: _injectFeatures2
    });
    this.isDev = void 0;
    this.isTest = void 0;
    this.side = void 0;
    Object.defineProperty(this, _webpackConfigBuilder, {
      writable: true,
      value: void 0
    });
    this.isDev = isDev;
    this.isTest = isTest;
    this.side = side;
    _classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder] = new WebpackConfigBuilder();

    _classPrivateFieldLooseBase(this, _injectFeatures)[_injectFeatures]();
  }

  isServer() {
    return this.side === WebpackBase.SIDE_SERVER;
  }

  buildConfig() {
    const EXTENSIONS = wcbUtils.getFileTypeExtensions(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder], WebpackConfigBuilder.FILE_TYPE_JS).map(ext => `.${ext}`);
    const config = {
      cache: true,
      name: this.isServer() ? 'Server' : 'Client',
      entry: { ..._classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder].getEntries(),
        main: _classPrivateFieldLooseBase(this, _getMainEntry)[_getMainEntry]()
      },
      output: _classPrivateFieldLooseBase(this, _getOutput)[_getOutput](),
      module: {
        rules: _classPrivateFieldLooseBase(this, _buildRules)[_buildRules]()
      },
      plugins: _classPrivateFieldLooseBase(this, _getPlugins)[_getPlugins](),
      // Allow error boundaries to display errors in non-main chunks
      // https://reactjs.org/docs/cross-origin-errors.html
      devtool: 'cheap-module-source-map',
      performance: {
        hints: false
      },
      target: this.isServer() ? 'node' : 'web',
      node: {
        // don't transform __dirname when running inside Node.
        __dirname: !this.isServer()
      },
      infrastructureLogging: {
        level: this.isDev ? 'warn' : 'info'
      },
      stats: this.isDev ? 'errors-warnings' : 'normal',
      ignoreWarnings: [{
        message: /InjectManifest has been called multiple times/
      }],
      resolve: {
        modules: ['node_modules'],
        extensions: EXTENSIONS,
        mainFields: [// when loaded by webpack for the browser
        ...(this.isServer() ? ['server:main'] : ['browser:main']), // when loaded by webpack (node or browser)
        'webpack:main', 'jsnext:module', 'module', 'jsnext:main', 'main'],
        alias: _classPrivateFieldLooseBase(this, _getAliases)[_getAliases](),
        // they fucked up their package
        // @ts-expect-error
        plugins: [new ResolveTypescriptPlugin.default()]
      },
      mode: this.isDev ? 'development' : 'production',
      experiments: {
        topLevelAwait: true
      },
      optimization: {
        minimize: false,
        removeAvailableModules: false
      }
    };

    if (this.isServer()) {
      // exclude any absolute module (npm/node) from the build, except this module.
      // this module must be included otherwise the server build will have two
      // separate versions of the framework loaded.
      // const anyAbsoluteExceptFrameworkAndCss = new RegExp(`^(?!${}(\\/.+)?$)[a-z\\-_0-9]+(?!.+\\.css$).+$`);
      config.externals = [nodeExternals({
        allowlist: [// framework must be processed by webpack as it relies on some webpack processes
        new RegExp(`^${frameworkMetadata.name}`), // TODO: should we mark all plugins as non-externals?
        /^@reworkjs\/redux/, /\.css$/i]
      })];
    } else {
      config.resolve.mainFields.unshift('web');
      config.resolve.mainFields.unshift('jsnext:web');
      config.resolve.mainFields.unshift('browser');
      config.resolve.mainFields.unshift('jsnext:browser');
    }

    return wcbUtils.mergeRaw(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder], config);
  }

}

function _injectFeatures2() {
  logger.debug('Injecting webpack Features.');
  const requestedFeatures = parseFeatures(argv.features);
  const features = featureClasses.map(FeatureClass => new FeatureClass(this.isServer(), process.env.NODE_ENV));
  sortDependencies(features);

  for (const feature of features) {
    _classPrivateFieldLooseBase(this, _injectFeature)[_injectFeature](feature, requestedFeatures);
  }
}

function _injectFeature2(feature, requestedFeatures) {
  const name = feature.getFeatureName();
  const enabled = requestedFeatures[name] != null ? requestedFeatures[name] : feature.isDefaultEnabled();
  logger.debug(`${enabled ? chalkOk('✓') : chalkNok('✘')} Feature ${name}`);

  if (!enabled) {
    return false;
  }

  feature.visit(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder]);
  return true;
}

function _getMainEntry2() {
  // front-end entry point.
  const mainEntry = [this.isServer() ? resolveFrameworkSource('server/index') : resolveFrameworkSource('client/index')]; // front-end dev libs.

  if (this.isDev && !this.isServer()) {
    mainEntry.unshift( // Necessary for hot reloading with IE
    'eventsource-polyfill', 'webpack-hot-middleware/client', require.resolve('./dev-preamble'));
  } else if (this.isDev && this.isServer()) {
    mainEntry.unshift( // hot reload if parent sends signal SIGUSR2
    require.resolve('./hmr-server'));
  }

  return mainEntry;
}

function _getOutput2() {
  // Output to build directory.
  const output = getWebpackSettings(this.isServer()).output; // This allow error boundaries to display errors in non-main chunks
  // https://reactjs.org/docs/cross-origin-errors.html
  // It's also necessary for subresource-integrity

  output.crossOriginLoading = 'anonymous';

  if (this.isServer()) {
    output.libraryTarget = 'commonjs2';
  }

  if (this.isDev || this.isServer()) {
    // Don't use hashes in dev mode for better building performance.
    // Don't use them on the server because they're not needed and not user friendly.
    Object.assign(output, {
      filename: '[name].js',
      chunkFilename: '[name].chunk.js' // https://github.com/mxstbr/react-boilerplate/issues/443
      // publicPath: 'http://localhost:3000',

    });
  } else {
    // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets.
    Object.assign(output, {
      filename: '[name].[chunkhash].js',
      chunkFilename: '[name].[chunkhash].chunk.js'
    });
  }

  return output;
}

function _buildRules2() {
  const resources = [/\.(eot|ttf|woff|woff2)(\?.*$|$)/i, /\.(mp4|webm|md)/i, wcbUtils.getFileTypeRegExp(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder], WebpackConfigBuilder.FILE_TYPE_IMG)];
  return [...resources.map(resourceRegex => {
    return {
      oneOf: [// adding ?raw will include the source as string
      {
        type: 'asset/source',
        resourceQuery: /raw/
      }, {
        type: 'asset/resource',
        resourceQuery: /url/
      }, {
        type: 'asset/resource',
        test: resourceRegex
      }]
    };
  }), // HACK: (.codegen.cjs)
  //  val-loader files have to be .cjs because val-loader doesn't support running ESM files yet
  //  but we want to treat the cod generated by the CommonJS file as ESM
  //  so we tell webpack it's actually a ESM file (it only impacts the generated code, not the loader).
  //  https://github.com/webpack-contrib/val-loader/issues/80
  {
    test: /\.codegen.cjs$/i,
    type: 'javascript/esm'
  }].concat(_classPrivateFieldLooseBase(this, _buildCssLoaders)[_buildCssLoaders]()).concat(wcbUtils.buildRules(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder]));
}

function _getCssLoader2(options) {
  if (options === void 0) {
    options = {};
  }

  const loaderOptions = {
    importLoaders: options.importLoaders || 0 // sourceMap: depends on webpack's `devtool` config
    // module.auto is true by default

  };

  if (options.modules) {
    Object.assign(loaderOptions, {
      modules: {
        exportLocalsConvention: 'camelCase'
      }
    });

    if (isDev) {
      loaderOptions.modules.getLocalIdent = buildLocalIdentGenerator();
    } else {
      loaderOptions.modules.getLocalIdent = minifiedCssIdents();
    } // in prod with pre-rendering, we don't generate the CSS. Only the mapping "css class" => "css module class"
    // the actual CSS is served directly from the client bundle.


    if (this.isServer() && !this.isDev) {
      loaderOptions.modules.exportOnlyLocals = true;
    }
  }

  return {
    loader: 'css-loader',
    options: loaderOptions
  };
}

function _buildCssLoaders2() {
  const pluggedCssLoaders = wcbUtils.getCssLoaders(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder]);
  const cssLoaders = [{
    test: wcbUtils.getFileTypeRegExp(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder], WebpackConfigBuilder.FILE_TYPE_CSS),
    exclude: /node_modules/,
    use: [_classPrivateFieldLooseBase(this, _getCssLoader)[_getCssLoader]({
      modules: true,
      importLoaders: pluggedCssLoaders.length
    }), ...pluggedCssLoaders]
  }, {
    test: /\.css$/i,
    include: /node_modules/,
    use: [_classPrivateFieldLooseBase(this, _getCssLoader)[_getCssLoader]({
      modules: false
    })]
  }];

  const styleLoader = (() => {
    // server ignores built css, it sends the one built by the client
    if (this.isServer()) {
      return null;
    }

    return {
      // mini-css-extract-plugin is *very* slow and a no-go for watch mode
      loader: this.isDev ? 'style-loader' : ExtractCssPlugin.loader
    };
  })();

  if (styleLoader != null) {
    for (const cssLoader of cssLoaders) {
      cssLoader.use = [styleLoader, ...cssLoader.use];
    }
  }

  return cssLoaders;
}

function _getAliases2() {
  const frameworkAliases = {
    // Framework configuration directories
    '@@pre-init': frameworkConfig['pre-init'] || resolveFrameworkSource('dummy/empty-function.js'),
    '@@render-html': frameworkConfig['render-html'] || resolveFrameworkSource('server/setup-http-server/default-render-page.js'),
    '@@directories.translations': frameworkConfig.directories.translations,
    // Support React Native Web
    // https://www.smashingmagazine.com/2016/08/a-glimpse-into-the-future-with-react-native-for-web/
    'react-native': 'react-native-web',
    'react-helmet': resolveFrameworkSource('dummy/react-helmet.js')
  }; // if (this.isServer()) {
  //   // TODO:
  //   // force babel-runtime to load as commonjs on server because
  //   // node cannot load esm itself (babel-runtime is external)
  //   // --> If this doesn't work, exclude @babel/runtime/helpers/esm from externals
  //   frameworkAliases['@babel/runtime/helpers/esm'] = '@babel/runtime/helpers';
  // }

  const customAliases = wcbUtils.getAliases(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder]);
  Object.assign(frameworkAliases, customAliases);
  return frameworkAliases;
}

function _getDefinedVars2() {
  const NODE_ENV = JSON.stringify(process.env.NODE_ENV);
  const SIDE = JSON.stringify(this.isServer() ? 'server' : 'client');
  const definedVariables = {
    process: {
      env: {
        SIDE,
        NODE_ENV,
        PROCESS_NAME: JSON.stringify(`${projectMetadata.name} (${this.isServer() ? 'server' : 'client'})`)
      }
    },
    // TODO: replace with val-loader
    $$RJS_VARS$$: {
      FRAMEWORK_METADATA: JSON.stringify(frameworkMetadata),
      PROJECT_METADATA: JSON.stringify(projectMetadata),
      PARSED_ARGV: JSON.stringify(appArgv)
    }
  };

  if (this.isServer()) {
    const outputDirectory = getWebpackSettings(this.isServer()).output.path; // we only pass build & logs to bundled code
    // as they are the only folders that NEED to exist for the
    // app to run.
    // Using anything else MUST be an error.
    // We don't give any information to the client bundle as they cannot interact
    // with the filesystem at all and trying to MUST be an error too.
    // We give the path relative to the output directory so the project can be
    // moved around freely after being built. This can happen when updating a live app: The app will be built
    // in a temporary folder then moved to its final destination.
    // The path relative path will be transformed into an absolute path at runtime.
    // all executed JS files will be located at the root of outputDirectory, so we the new absolute path
    // will be the correct one no matter which bundle produces it.

    const FRAMEWORK_CONFIG = {
      directories: {
        logs: frameworkConfig.directories.logs ? path.relative(outputDirectory, frameworkConfig.directories.logs) : null,
        build: path.relative(outputDirectory, frameworkConfig.directories.build)
      }
    }; // TODO: replace with val-loader (or remove)

    definedVariables.$$RJS_VARS$$.FRAMEWORK_CONFIG = JSON.stringify(FRAMEWORK_CONFIG);
  }

  return flattenKeys(definedVariables);
}

function _getPlugins2() {
  // TODO inject DLLs <script data-dll='true' src='/${dllName}.dll.js'></script>`
  // TODO https://github.com/diurnalist/chunk-manifest-webpack-plugin
  const plugins = [new WebpackBar({
    name: this.isServer() ? 'Server Bundle' : 'Client Bundle',
    color: this.isServer() ? 'blue' : 'green'
  }), // remove outdated assets from previous builds.
  new CleanWebpackPlugin(), new webpack.DefinePlugin(_classPrivateFieldLooseBase(this, _getDefinedVars)[_getDefinedVars]()), new CopyWebpackPlugin({
    patterns: [{
      noErrorOnMissing: true,
      from: frameworkConfig.directories.resources,
      to: './',
      toType: 'dir',
      globOptions: {
        dot: true
      }
    }]
  }), // subresource-integrity
  new SriPlugin({
    hashFuncNames: ['sha384'],
    // FIXME SRI disabled due to it outputting the wrong values
    enabled: false // frameworkConfig['emit-integrity'] && !this.isDev,

  }), // Inject webpack bundle into HTML.
  new HtmlWebpackPlugin({
    inject: true,
    templateContent: buildIndexPage(),
    // FIXME temporary hack for webpack 4 https://github.com/jantimon/html-webpack-plugin/issues/870
    chunksSortMode: 'none',
    minify: this.isDev ? false : {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true
    }
  }), // Dump chunk mapping and entryPoints so we can determine which client chunks to send depending on which
  // chunks were imported during server-side rendering
  new LoadablePlugin({
    writeToDisk: true
  })]; // if (!this.isServer()) {
  //   plugins.push(
  //     new PolyfillInjectorPlugin({
  //       polyfills: ['Promise'],
  //     }),
  //   );
  // }

  if (this.isDev) {
    plugins.push( // enable hot reloading.
    new webpack.HotModuleReplacementPlugin(), // react-refresh is disabled because of how incredibly slow it is since webpack 5
    // import ReactRefreshPlugin from '@pmmmwh/react-refresh-webpack-plugin';
    // new ReactRefreshPlugin(),
    // Watcher doesn't work well if you mistype casing in a path so we use
    // a plugin that prints an error when you attempt to do this.
    // See https://github.com/facebookincubator/create-react-app/issues/240
    new CaseSensitivePathsPlugin());
  }

  if (!this.isServer() && !this.isDev) {
    // Extract the CSS into a separate file.
    plugins.push(new ExtractCssPlugin({
      filename: this.isDev ? '[name].css' : '[name].[contenthash].css',
      chunkFilename: this.isDev ? '[id].css' : '[id].[contenthash].css',
      // in order to generate the CSS files properly, import order must
      // be the same across the project. It is very hard / impossible to
      // do that by hand currently. Until an automated solution arrives,
      // this warning is useless.
      ignoreOrder: this.isDev
    }));
  }

  return plugins.concat(...wcbUtils.getPlugins(_classPrivateFieldLooseBase(this, _webpackConfigBuilder)[_webpackConfigBuilder]));
}

WebpackBase.SIDE_SERVER = 0;
WebpackBase.SIDE_CLIENT = 1;

function buildIndexPage() {
  const helmetContext = {};
  const body = renderToString(_jsxDEV(HelmetProvider, {
    context: helmetContext,
    children: _jsxDEV(BaseHelmet, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 567,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 566,
    columnNumber: 5
  }, this));
  assert(helmetContext.helmet != null);
  return renderPage({
    body,
    helmet: helmetContext.helmet
  });
}

function flattenKeys(obj, out, paths) {
  if (out === void 0) {
    out = {};
  }

  if (paths === void 0) {
    paths = [];
  }

  for (const key of Object.getOwnPropertyNames(obj)) {
    paths.push(key);

    if (typeof obj[key] === 'object') {
      flattenKeys(obj[key], out, paths);
    } else {
      out[paths.join('.')] = obj[key];
    }

    paths.pop();
  }

  return out;
}
/**
 *
 * @returns {function(*, *, *)}
 */


function buildLocalIdentGenerator() {
  const prefixMap = new Map();
  const existingPrefixes = new Set(); // alternative solution for prefix gen: take file path and remove common junk (src, style, etc...)
  //  eg. src/components/Button/style.module.scss -> components-Button
  // ignore fileName if it is the same as its parent folder
  //  eg. src/views/home/home.module.scss -> views-home (not views-home-home)
  // remove extension from fileNames
  //  eg. src/global.module.scss -> global
  // but keep fileName if there is no folder, even if junk
  //  eg. src/styles.scss -> styles
  // const junkPrefixes = new Set(['src', 'lib', 'app', 'component', 'components']);
  // const junkFileNames = new Set(['styles', 'style', 'css', 'index']); // fileNames ignore everything after first dot

  function getPrefix(filePath) {
    if (!prefixMap.has(filePath)) {
      const folderMatch = filePath.match(/\/([^/]+)\/[^/]+$/);
      const prefixBase = folderMatch[1];
      let uniquePrefix = prefixBase;
      let i = 1;

      while (existingPrefixes.has(uniquePrefix)) {
        uniquePrefix = prefixBase + i;
        i++;
      }

      existingPrefixes.add(uniquePrefix);
      prefixMap.set(filePath, uniquePrefix);
    }

    return prefixMap.get(filePath);
  }

  return (context, _, localName) => {
    const prefix = getPrefix(context.resourcePath);
    return `${prefix}__${localName}`;
  };
}