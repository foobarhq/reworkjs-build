import assert from 'assert';
import path from 'path';
import logger from '@reworkjs/core/logger';
import { chalkUrl } from '../../shared/chalk.js';
import compileWebpack from './compile-webpack.js';
import config from './webpack.server.js';
logger.info('Building your server-side app, this might take a minute.');
const WATCH = process.env.WATCH === 'true';
compileWebpack(config, WATCH, stats => {
  var _stats$entrypoints, _mainEntryPoint$asset;

  const mainEntryPoint = (_stats$entrypoints = stats.entrypoints) == null ? void 0 : _stats$entrypoints.main;
  assert(mainEntryPoint != null);
  const entryPoints = ((_mainEntryPoint$asset = mainEntryPoint.assets) == null ? void 0 : _mainEntryPoint$asset.filter(asset => {
    const fileName = asset.name;

    if (!fileName.endsWith('.js')) {
      return false;
    } // ignore hot updates for now


    return !fileName.endsWith('.hot-update.js');
  })) ?? [];

  if (entryPoints.length !== 1) {
    logger.debug('entry points:');
    logger.debug(entryPoints);
    throw new Error('Webpack built but the output does not have exactly one entry point. This is a bug.');
  }

  const entryPoint = path.join(config.output.path, entryPoints[0].name); // don't need to know about the entry point if it is handled by another process automatically.

  const method = process.send ? 'debug' : 'info';
  logger[method](`Server entry point: ${chalkUrl(entryPoint)}`); // tell manager CLI to launch server

  if (process.send) {
    process.send({
      cmd: 'built',
      exe: entryPoint
    });
  }
});