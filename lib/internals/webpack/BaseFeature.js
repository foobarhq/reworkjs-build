import logger from '@reworkjs/core/logger';
import { chalkCommand, chalkWebpackFeature, chalkNpmDep } from '../../shared/chalk.js';
import WebpackConfigBuilder from './WebpackConfigBuilder.js';
export default class BaseFeature {
  constructor(isServer, env) {
    this._isServer = void 0;
    this._env = void 0;
    this._isServer = isServer;
    this._env = env;
  }

  loadAfter() {
    return null;
  }

  loadBefore() {
    return null;
  }

  isDefaultEnabled() {
    return true;
  }

  isDev() {
    return !this.isProd();
  }

  getEnv() {
    return this._env || 'production';
  }

  getSideName() {
    return this.isServer() ? 'SSR' : 'Client';
  }

  isProd() {
    return this.getEnv() === 'production';
  }

  isServer() {
    return this._isServer;
  }

  isClient() {
    return !this.isServer();
  }

  visit(webpack) {
    // eslint-disable-line
    throw new Error('visit not implemented');
  }

  getOptionalDependency(dependencyName) {
    try {
      return require(dependencyName);
    } catch (e) {
      if (e.code !== 'MODULE_NOT_FOUND') {
        throw e;
      }

      logger.error(`Feature ${chalkWebpackFeature(this.getFeatureName())} is missing the dependency ${chalkNpmDep(dependencyName)}. run ${chalkCommand(`npm install ${dependencyName}`)}`);
      throw e;
    }
  }

}
BaseFeature.FILE_TYPE_JS = WebpackConfigBuilder.FILE_TYPE_JS;
BaseFeature.FILE_TYPE_CSS = WebpackConfigBuilder.FILE_TYPE_CSS;
BaseFeature.FILE_TYPE_IMG = WebpackConfigBuilder.FILE_TYPE_IMG;