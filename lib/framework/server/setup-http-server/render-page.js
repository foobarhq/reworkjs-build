import loadRenderPage from '@reworkjs/core/_internal_/render-html';

class Head {
  constructor(data) {
    this.title = void 0;
    this.base = void 0;
    this.meta = void 0;
    this.link = void 0;
    this.style = void 0;
    this.script = void 0;
    this.noscript = void 0;
    this.end = void 0;
    Object.assign(this, data);
  }

  toString() {
    return this.title + this.base + this.meta + this.link + this.style + this.script + this.noscript + this.end;
  }

}

class Body {
  constructor(start, app, end) {
    this.start = void 0;
    this.app = void 0;
    this.end = void 0;
    this.start = start;
    this.app = app;
    this.end = end;
  }

  toString() {
    return this.start + this.app + this.end;
  }

}

const renderPageDelegate = await loadRenderPage();
export default function renderPage(data) {
  const {
    helmet
  } = data;
  const pageHtml = {
    head: new Head({
      title: stringifyHelmet(helmet.title),
      base: stringifyHelmet(helmet.base),
      meta: stringifyHelmet(helmet.meta),
      link: stringifyHelmet(helmet.link),
      style: stringifyHelmet(helmet.style),
      script: stringifyHelmet(helmet.script),
      noscript: stringifyHelmet(helmet.noscript),
      end: data.header || ''
    }),
    body: new Body('', `<div id="app">${data.body || ''}</div>`, data.footer || ''),
    bodyAttributes: stringifyHelmet(helmet.bodyAttributes),
    htmlAttributes: stringifyHelmet(helmet.htmlAttributes)
  };
  return renderPageDelegate(pageHtml);
}

function stringifyHelmet(part) {
  if (!part || !part.toString) {
    return '';
  }

  return part.toString();
}