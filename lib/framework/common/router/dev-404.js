var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/common/router/dev-404.tsx";
import * as React from 'react';
import { useLocation } from 'react-router-dom';
import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";
import { Fragment as _Fragment } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";
export default function Error404View() {
  const loc = useLocation();
  return _jsxDEV(_Fragment, {
    children: [_jsxDEV("style", {
      type: "text/css",
      children: `
          .container {
            max-width: 600px;
            padding: 8px;
            margin: 0 auto;
            width: 100%;

            line-height: 1.6;
            font-size: 18px;
            color: #444;
          }

          p {
            font-family: 'sans-serif';
          }

          code {
            font-family: 'monospace';
            padding: 0.2em 0.4em;
            margin: 0;
            background: rgba(27,31,35,0.05);
            border-radius: 3px;
          }

          .footnote {
            font-size: 12px;
            text-align: right;
          }
        `
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 7
    }, this), _jsxDEV("div", {
      className: "container",
      children: [_jsxDEV("h1", {
        children: "rework.js development 404"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 9
      }, this), _jsxDEV("p", {
        children: ["You are seeing this page because there is no page at ", _jsxDEV("code", {
          children: loc.pathname
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 65
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 9
      }, this), _jsxDEV("p", {
        children: _jsxDEV("a", {
          href: "https://www.reworkjs.com/routing",
          target: "_blank",
          rel: "noopener noreferrer",
          children: "Read up on how to create a new page"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 12
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 9
      }, this), _jsxDEV("p", {
        className: "footnote",
        children: _jsxDEV("em", {
          children: "This message in only visible in development"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 46,
          columnNumber: 33
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 7
    }, this)]
  }, void 0, true);
}