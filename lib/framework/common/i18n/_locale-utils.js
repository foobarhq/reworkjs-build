export function getLocaleBestFit(locale, availableLocales) {
  if (availableLocales.includes(locale)) {
    return locale;
  }

  if (!locale.includes('-')) {
    return null;
  }

  const countryPart = locale.split('-')[0];

  if (availableLocales.includes(countryPart)) {
    return countryPart;
  }

  return null;
}
export function getFileName(file) {
  var _file$match;

  const fileName = (_file$match = file.match(/^\.\/(.+)\..+$/)) == null ? void 0 : _file$match[1];

  if (fileName == null) {
    throw new Error(`Could not extract fileName from ${file}`);
  }

  return fileName;
}