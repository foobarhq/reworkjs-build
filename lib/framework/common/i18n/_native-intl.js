import { shouldPolyfill as shouldPolyfillListFormat } from '@formatjs/intl-listformat/should-polyfill.js';
import { shouldPolyfill as shouldPolyfillNumber } from '@formatjs/intl-numberformat/should-polyfill.js';
import { shouldPolyfill as shouldPolyfillPlural } from '@formatjs/intl-pluralrules/should-polyfill.js';
import { shouldPolyfill as shouldPolyfillRelativeTime } from '@formatjs/intl-relativetimeformat/should-polyfill.js';
import { getListFormatLoaders, getNumberLocaleLoaders, getPluralRulesLocaleLoaders, getRelativeTimeLocaleLoaders } from '@reworkjs/core/_internal_/translations';
import { getFileName, getLocaleBestFit } from './_locale-utils.js';

async function _loadPolyfillLocale(localeLoaders, localeName) {
  const availableIntlLocales = localeLoaders.keys().map(getFileName);
  let actualLocale = getLocaleBestFit(localeName, availableIntlLocales);

  if (actualLocale == null) {
    console.error(`Could not fetch Unified Number Format locale '${localeName}'. Fallback to 'en'.`); // TODO(DEFAULT_LOCALE): use default locale instead of 'en'

    actualLocale = 'en';
  }

  const Module = await localeLoaders(`./${actualLocale}.js`);
  return Module.default;
} // ===================== LIST FORMAT  =====================


async function installListFormat(localeName) {
  if (!shouldPolyfillListFormat(localeName)) {
    return;
  }

  await import(
  /* webpackChunkName: "p-intllist" */
  '@formatjs/intl-listformat/polyfill.js');
  await _loadPolyfillLocale(getListFormatLoaders(), localeName);
} // ===================== NUMBER FORMAT  =====================


async function installUnifiedNumberFormat(localeName) {
  // install polyfill if unified number format is not natively supported
  if (!shouldPolyfillNumber(localeName)) {
    return;
  }

  await import(
  /* webpackChunkName: "p-intlunit" */
  '@formatjs/intl-numberformat/polyfill.js');
  await _loadPolyfillLocale(getNumberLocaleLoaders(), localeName);
} // ===================== PLURAL RULES  =====================


async function installPluralRules(localeName) {
  if (!shouldPolyfillPlural(localeName)) {
    return;
  }

  await import(
  /* webpackChunkName: "p-intlplural" */
  '@formatjs/intl-pluralrules/polyfill.js');
  await _loadPolyfillLocale(getPluralRulesLocaleLoaders(), localeName);
} // ===================== RELATIVE TIME =====================


async function installRelativeTime(localeName) {
  if (!shouldPolyfillRelativeTime(localeName)) {
    return;
  }

  await _loadPolyfillLocale(getRelativeTimeLocaleLoaders(), localeName);
} // ====================================================================


export async function installIntlLocale(localeName) {
  return Promise.all([installUnifiedNumberFormat(localeName), installPluralRules(localeName), installRelativeTime(localeName), installListFormat(localeName)]).then(() => void 0);
}