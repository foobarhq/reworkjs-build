const hotReloadListeners = new Set();
export function onIntlHotReload(callback) {
  hotReloadListeners.add(callback);
  return () => offIntlHotReload(callback);
}
export function offIntlHotReload(callback) {
  hotReloadListeners.delete(callback);
}
export function triggerI18nHotReload() {
  hotReloadListeners.forEach(callback => callback());
}