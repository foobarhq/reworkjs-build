var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/common/ReworkRootComponent.tsx";
import BaseHelmet from './BaseHelmet.js';
import LanguageComponent from './LanguageComponent.js';
import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";
import { Fragment as _Fragment } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";

function ReworkRootComponent(props) {
  return _jsxDEV(_Fragment, {
    children: [_jsxDEV(BaseHelmet, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }, this), _jsxDEV(LanguageComponent, {
      children: props.children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this)]
  }, void 0, true);
}

export default ReworkRootComponent;