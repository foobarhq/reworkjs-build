var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/common/kernel.tsx";
import { Switch } from 'react-router-dom';
import MainComponent from './app-main-component/index.js';
import debug from './debug.js';
import createRoutes from './router/create-routes.js'; // Set up the router, wrapping all Routes in the App component

import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";
const topLevelRoutes = createRoutes();
debug.topLevelRoutes = topLevelRoutes;

const rootRoute = _jsxDEV(MainComponent, {
  children: _jsxDEV(Switch, {
    children: topLevelRoutes
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 5
  }, this)
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 13,
  columnNumber: 3
}, this);

export { rootRoute };