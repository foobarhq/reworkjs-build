var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/common/LanguageComponent.tsx";
import { useAcceptLanguage } from '@reworkjs/core/ssr';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useCookies } from 'react-cookie';
import { IntlProvider } from 'react-intl';
import { EMPTY_OBJECT } from '../util/utils.js';
import { ActiveLocaleContext } from './active-locale-context.js';
import { isTranslationSupported } from './i18n/_app-translations.js';
import { guessPreferredLocale, LOCALE_COOKIE_NAME } from './i18n/get-preferred-locale.js';
import { onIntlHotReload, installLocale } from './i18n/index.js';
import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";

/**
 * this component synchronizes the internal i18n state with react-intl.
 */
export default function LanguageComponent(props) {
  const acceptLanguages = useAcceptLanguage();
  const [cookies, setCookie] = useCookies([LOCALE_COOKIE_NAME]);
  const localeCookie = cookies[LOCALE_COOKIE_NAME]; // TODO(DEFAULT_LOCALE): use default locale instead of 'en'

  const [activeLocale, setActiveLocale] = useState('en');
  const [messages, setMessages] = useState(EMPTY_OBJECT);
  const forceUpdate = useForceUpdate();
  useEffect(() => {
    const initialLocale = guessPreferredLocale(localeCookie, acceptLanguages); // TODO: warn on error?

    void installLocale(initialLocale).then(data => {
      setActiveLocale(initialLocale);
      setMessages(data.messages);
    }); // we only want to run this once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); // eslint-disable-next-line consistent-return

  useEffect(() => {
    // @ts-expect-error Webpack internal
    if (import.meta.webpackHot) {
      return onIntlHotReload(() => {
        void installLocale(activeLocale).then(() => forceUpdate());
      });
    }
  }, [activeLocale, forceUpdate]);
  const setNewActiveLocale = useCallback(async newLocale => {
    if (!isTranslationSupported(newLocale)) {
      throw new Error(`Locale ${newLocale} is unsupported`);
    }

    if (localeCookie !== newLocale) {
      setCookie(LOCALE_COOKIE_NAME, newLocale, {
        path: '/'
      });
    }

    return installLocale(newLocale).then(data => {
      setActiveLocale(newLocale);
      setMessages(data.messages);
    });
  }, [localeCookie, setCookie]);
  const activeLocaleContext = useMemo(() => {
    return [activeLocale, setNewActiveLocale];
  }, [activeLocale, setNewActiveLocale]);
  return _jsxDEV(IntlProvider, {
    locale: activeLocale // mute very annoying errors from react-intl
    // it's ok for messages to be missing during development.
    // add a CI check to ensure your localisation files are not missing a key or doesn't
    // contain an empty message during publish.
    ,
    defaultLocale: activeLocale,
    messages: messages,
    children: _jsxDEV(ActiveLocaleContext.Provider, {
      value: activeLocaleContext,
      children: props.children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 76,
    columnNumber: 5
  }, this);
}

function useForceUpdate() {
  const [, setDummy] = useState(0);
  return useCallback(() => {
    setDummy(dummy => dummy + 1);
  }, []);
}