import { useRef } from 'react';
export function usePersistentValue(_key, params) {
  // TODO: add support for deserializing
  return useRef(params.init()).current;
}