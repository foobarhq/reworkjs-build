import { useEffect, useState } from 'react';
import { loadResource } from './load-resource.js';
export function useAsyncResource(key, fetchCallback) {
  const [status, setStatus] = useState({
    loading: true,
    value: void 0,
    error: void 0
  });
  useEffect(() => {
    void loadResource(fetchCallback).then(newStatus => {
      setStatus(newStatus);
    });
  }, [fetchCallback, key]);
  return status;
}