export async function loadResource(fetchCallback) {
  try {
    const value = await fetchCallback();
    return {
      loading: false,
      value,
      error: void 0
    };
  } catch (e) {
    return {
      loading: false,
      value: void 0,
      error: e
    };
  }
}