var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/common/router-api.tsx";
import { Route } from 'react-router-dom';
import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";
export function HttpStatus(props) {
  return _jsxDEV(Route, {
    render: _ref => {
      let {
        staticContext
      } = _ref;

      if (staticContext) {
        staticContext.statusCode = props.code;
      }

      return props.children == null ? null : props.children;
    }
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 5
  }, this);
}