import { createContext, useContext } from 'react';

/**
 * This context contains the currently active locale for the application, and a function to change it.
 */
export const ActiveLocaleContext = /*#__PURE__*/createContext([// TODO(DEFAULT_LOCALE): use default locale instead of 'en'
'en', _newLocale => {}]);
export function useActiveLocale() {
  return useContext(ActiveLocaleContext);
}