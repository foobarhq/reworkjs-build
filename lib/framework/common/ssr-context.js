import { createContext } from 'react';

/**
 * This context contains the request and response objects of the current HTTP request. Server-side only. (empty object on client side).
 */
export const SsrContext = /*#__PURE__*/createContext(Object.freeze({}));