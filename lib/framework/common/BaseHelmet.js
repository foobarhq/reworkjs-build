var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/common/BaseHelmet.tsx";
import { useAcceptLanguage } from '@reworkjs/core/ssr';
import { useCookies } from 'react-cookie';
import { Helmet } from 'react-helmet-async';
import { guessPreferredLocale, LOCALE_COOKIE_NAME } from './i18n/get-preferred-locale.js';
import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";

function BaseHelmet() {
  // if process.env.SIDE is null, we're the build process. Use default value.
  // TODO(DEFAULT_LOCALE): use default locale instead of 'en'
  let lang = 'en';

  if (process.env.SIDE == null) {
    // .SIDE is immutable
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const acceptLanguages = useAcceptLanguage(); // eslint-disable-next-line react-hooks/rules-of-hooks

    const [cookies] = useCookies([LOCALE_COOKIE_NAME]);
    lang = getLangFromLocale(guessPreferredLocale(cookies[LOCALE_COOKIE_NAME], acceptLanguages));
  }

  return _jsxDEV(Helmet, {
    children: [_jsxDEV("html", {
      lang: lang
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), _jsxDEV("meta", {
      charSet: "utf-8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }, this), _jsxDEV("meta", {
      name: "viewport",
      content: "width=device-width, initial-scale=1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }, this), _jsxDEV("meta", {
      name: "mobile-web-app-capable",
      content: "yes"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 21,
    columnNumber: 5
  }, this);
}

function getLangFromLocale(locale) {
  return locale.split(/[-_]/)[0];
} // only inject cookies if we're in HTTP server or the browser context
// not if we're the builder.


export default BaseHelmet;