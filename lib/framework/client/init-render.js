var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/core/src/framework/client/init-render.tsx";
import { loadableReady } from '@loadable/component';
import serverStyleCleanup from 'node-style-loader/clientCleanup.js';
import ReactDOM from 'react-dom';
import { updateServiceWorker } from '../common/service-worker-updater.js';
import RootComponent from './root-component.js';
import { jsxDEV as _jsxDEV } from "@reworkjs/core/_internal_/react/jsx-dev-runtime";
const appContainer = document.getElementById('app');

if (appContainer.hasChildNodes()) {
  // ensure all needed chunks are loaded before hydrating to prevent flicker
  loadableReady(() => {
    ReactDOM.hydrate(_jsxDEV(RootComponent, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 7
    }, this), appContainer);
  });
} else {
  ReactDOM.render(_jsxDEV(RootComponent, {}, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 18,
    columnNumber: 5
  }, this), appContainer);
} // remove server-generated CSS


serverStyleCleanup();
updateServiceWorker();