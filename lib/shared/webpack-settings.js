import frameworkConfig from '@reworkjs/core/_internal_/framework-config';
export default function getWebpackSettings(server) {
  return {
    output: {
      path: `${frameworkConfig.directories.build}/${server ? 'server' : 'client'}`,
      publicPath: '/'
    }
  };
}