import levels from './_levels.js';

class Logger {}

for (const levelName of Object.keys(levels)) {
  // const levelImportance = levels[levelName];
  const loggingMethod = console[levelName.toLocaleLowerCase()] || console.log; // eslint-disable-line no-console

  Logger.prototype[levelName] = function logMessage() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    loggingMethod.call(console, `[${levelName}]`, ...args);
  };
}

export default new Logger();
export { levels };