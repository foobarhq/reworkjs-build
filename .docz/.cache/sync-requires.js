const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---license-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/LICENSE.md"))),
  "component---readme-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/README.md"))),
  "component---docs-1-getting-started-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/1-getting-started.md"))),
  "component---docs-2-routing-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/2-routing.md"))),
  "component---docs-3-styling-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/3-styling.md"))),
  "component---docs-4-public-resources-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/4-public-resources.md"))),
  "component---docs-5-i-18-n-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/5-i18n.md"))),
  "component---docs-6-page-head-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/6-page-head.md"))),
  "component---docs-api-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/api.md"))),
  "component---docs-cli-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/cli.md"))),
  "component---docs-server-side-rendering-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/server-side-rendering.md"))),
  "component---docs-advanced-topics-build-time-parameters-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/advanced-topics/build-time-parameters.md"))),
  "component---docs-advanced-topics-configuration-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/advanced-topics/configuration.md"))),
  "component---docs-advanced-topics-plugins-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/advanced-topics/plugins.md"))),
  "component---docs-advanced-topics-preact-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/advanced-topics/preact.md"))),
  "component---docs-advanced-topics-pwa-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/advanced-topics/pwa.md"))),
  "component---docs-javascript-flavors-custom-babelrc-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/javascript-flavors/custom-babelrc.md"))),
  "component---docs-javascript-flavors-flow-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/javascript-flavors/flow.md"))),
  "component---docs-javascript-flavors-typescript-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/docs/javascript-flavors/typescript.md"))),
  "component---changelog-md": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/CHANGELOG.md"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/ephys/Documents/dev/reworkjs/core/.docz/src/pages/404.js")))
}

