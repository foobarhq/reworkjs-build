// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---license-md": () => import("./../../LICENSE.md" /* webpackChunkName: "component---license-md" */),
  "component---readme-md": () => import("./../../README.md" /* webpackChunkName: "component---readme-md" */),
  "component---docs-1-getting-started-md": () => import("./../../docs/1-getting-started.md" /* webpackChunkName: "component---docs-1-getting-started-md" */),
  "component---docs-2-routing-md": () => import("./../../docs/2-routing.md" /* webpackChunkName: "component---docs-2-routing-md" */),
  "component---docs-3-styling-md": () => import("./../../docs/3-styling.md" /* webpackChunkName: "component---docs-3-styling-md" */),
  "component---docs-4-public-resources-md": () => import("./../../docs/4-public-resources.md" /* webpackChunkName: "component---docs-4-public-resources-md" */),
  "component---docs-5-i-18-n-md": () => import("./../../docs/5-i18n.md" /* webpackChunkName: "component---docs-5-i-18-n-md" */),
  "component---docs-6-page-head-md": () => import("./../../docs/6-page-head.md" /* webpackChunkName: "component---docs-6-page-head-md" */),
  "component---docs-api-md": () => import("./../../docs/api.md" /* webpackChunkName: "component---docs-api-md" */),
  "component---docs-cli-md": () => import("./../../docs/cli.md" /* webpackChunkName: "component---docs-cli-md" */),
  "component---docs-server-side-rendering-md": () => import("./../../docs/server-side-rendering.md" /* webpackChunkName: "component---docs-server-side-rendering-md" */),
  "component---docs-advanced-topics-build-time-parameters-md": () => import("./../../docs/advanced-topics/build-time-parameters.md" /* webpackChunkName: "component---docs-advanced-topics-build-time-parameters-md" */),
  "component---docs-advanced-topics-configuration-md": () => import("./../../docs/advanced-topics/configuration.md" /* webpackChunkName: "component---docs-advanced-topics-configuration-md" */),
  "component---docs-advanced-topics-plugins-md": () => import("./../../docs/advanced-topics/plugins.md" /* webpackChunkName: "component---docs-advanced-topics-plugins-md" */),
  "component---docs-advanced-topics-preact-md": () => import("./../../docs/advanced-topics/preact.md" /* webpackChunkName: "component---docs-advanced-topics-preact-md" */),
  "component---docs-advanced-topics-pwa-md": () => import("./../../docs/advanced-topics/pwa.md" /* webpackChunkName: "component---docs-advanced-topics-pwa-md" */),
  "component---docs-javascript-flavors-custom-babelrc-md": () => import("./../../docs/javascript-flavors/custom-babelrc.md" /* webpackChunkName: "component---docs-javascript-flavors-custom-babelrc-md" */),
  "component---docs-javascript-flavors-flow-md": () => import("./../../docs/javascript-flavors/flow.md" /* webpackChunkName: "component---docs-javascript-flavors-flow-md" */),
  "component---docs-javascript-flavors-typescript-md": () => import("./../../docs/javascript-flavors/typescript.md" /* webpackChunkName: "component---docs-javascript-flavors-typescript-md" */),
  "component---changelog-md": () => import("./../../CHANGELOG.md" /* webpackChunkName: "component---changelog-md" */),
  "component---src-pages-404-js": () => import("./../src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */)
}

