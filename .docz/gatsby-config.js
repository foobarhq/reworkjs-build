const { mergeWith } = require('lodash/fp')
const fs = require('fs-extra')

let custom = {}
const hasGatsbyConfig = fs.existsSync('./gatsby-config.custom.js')

if (hasGatsbyConfig) {
  try {
    custom = require('./gatsby-config.custom')
  } catch (err) {
    console.error(
      `Failed to load your gatsby-config.js file : `,
      JSON.stringify(err),
    )
  }
}

const config = {
  pathPrefix: '/',

  siteMetadata: {
    title: 'rework.js',
    description: 'A react framework that works out of the box',
  },
  plugins: [
    {
      resolve: 'gatsby-theme-docz',
      options: {
        themeConfig: {},
        themesDir: 'src',
        mdxExtensions: ['.md', '.mdx'],
        docgenConfig: {},
        menu: [
          'Introduction',
          'Getting Started',
          'Routing',
          'CSS & Styling',
          'Public Resources',
          'Page Metadata (head)',
          'Internationalization (i18n)',
          'Server Side Rendering',
          {
            name: 'JavaScript flavors',
            menu: ['TypeScript', 'Flow', 'Babel Config'],
          },
          {
            name: 'Advanced Topics',
            menu: ['Configuration', 'Plugins', 'Preact'],
          },
          'API',
          'Command Line Interface',
        ],
        mdPlugins: [],
        hastPlugins: [],
        ignore: [
          '.docz/**/*',
          '.idea/**/*',
          '.git/**/*',
          'src/shared/README.md',
          'src/framework/dummy/README.md',
          'CODE_OF_CONDUCT.md',
          'ROADMAP.md',
          'STRUCTURE.md',
          'START.md',
          'lib/**/*',
          'es/**/*',
        ],
        typescript: false,
        ts: false,
        propsParser: true,
        'props-parser': true,
        debug: false,
        native: false,
        openBrowser: false,
        o: false,
        open: false,
        'open-browser': false,
        root: '/Users/ephys/Documents/dev/reworkjs/core/.docz',
        base: '/',
        source: './',
        src: './',
        files: '**/*.{md,markdown,mdx}',
        public: '/public',
        dest: '.docz/dist',
        d: '.docz/dist',
        editBranch: 'master',
        eb: 'master',
        'edit-branch': 'master',
        config: '',
        title: 'rework.js',
        description: 'A react framework that works out of the box',
        host: 'localhost',
        port: 3000,
        p: 3000,
        separator: '-',
        paths: {
          root: '/Users/ephys/Documents/dev/reworkjs/core',
          templates:
            '/Users/ephys/Documents/dev/reworkjs/core/node_modules/docz-core/dist/templates',
          docz: '/Users/ephys/Documents/dev/reworkjs/core/.docz',
          cache: '/Users/ephys/Documents/dev/reworkjs/core/.docz/.cache',
          app: '/Users/ephys/Documents/dev/reworkjs/core/.docz/app',
          appPackageJson:
            '/Users/ephys/Documents/dev/reworkjs/core/package.json',
          gatsbyConfig:
            '/Users/ephys/Documents/dev/reworkjs/core/gatsby-config.js',
          gatsbyBrowser:
            '/Users/ephys/Documents/dev/reworkjs/core/gatsby-browser.js',
          gatsbyNode: '/Users/ephys/Documents/dev/reworkjs/core/gatsby-node.js',
          gatsbySSR: '/Users/ephys/Documents/dev/reworkjs/core/gatsby-ssr.js',
          importsJs:
            '/Users/ephys/Documents/dev/reworkjs/core/.docz/app/imports.js',
          rootJs: '/Users/ephys/Documents/dev/reworkjs/core/.docz/app/root.jsx',
          indexJs:
            '/Users/ephys/Documents/dev/reworkjs/core/.docz/app/index.jsx',
          indexHtml:
            '/Users/ephys/Documents/dev/reworkjs/core/.docz/app/index.html',
          db: '/Users/ephys/Documents/dev/reworkjs/core/.docz/app/db.json',
        },
      },
    },
  ],
}

const merge = mergeWith((objValue, srcValue) => {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue)
  }
})

module.exports = merge(config, custom)
